package com.toantd8.profile.mapper;

import com.toantd8.profile.dto.request.UserProfileCreationRequest;
import com.toantd8.profile.dto.response.UserProfileResponse;
import com.toantd8.profile.entity.UserProfile;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserProfileMapper {
    UserProfile toUserProfile(UserProfileCreationRequest request);
    UserProfileResponse toUserProfileResponse(UserProfile entity);
    void updateProfile(@MappingTarget UserProfile userProfile, UserProfileCreationRequest request);
}
