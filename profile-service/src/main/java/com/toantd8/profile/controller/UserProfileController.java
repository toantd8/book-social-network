package com.toantd8.profile.controller;

import com.toantd8.profile.dto.request.UserProfileCreationRequest;
import com.toantd8.profile.dto.response.UserProfileResponse;
import com.toantd8.profile.service.UserProfileService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
public class UserProfileController {
    UserProfileService userProfileService;

    @GetMapping("/{profileId}")
    public UserProfileResponse getProfile(@PathVariable String profileId) {
        return userProfileService.getProfile(profileId);
    }

    @GetMapping("/users")
    public List<UserProfileResponse> getProfileList() {
        return userProfileService.getProfileList();
    }

    @DeleteMapping("/{id}")
    public String deleteProfile(@PathVariable String id) {
        userProfileService.deleteProfile(id);
        return "Delete profile successfully";
    }

    @PutMapping("/{id}")
    public UserProfileResponse updateProfile(@PathVariable String id,
                                             @RequestBody UserProfileCreationRequest updateData) {
        return userProfileService.updateProfile(id, updateData);
    }
}
