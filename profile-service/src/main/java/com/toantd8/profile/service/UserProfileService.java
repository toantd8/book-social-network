package com.toantd8.profile.service;

import com.toantd8.profile.dto.request.UserProfileCreationRequest;
import com.toantd8.profile.dto.response.UserProfileResponse;
import com.toantd8.profile.entity.UserProfile;
import com.toantd8.profile.mapper.UserProfileMapper;
import com.toantd8.profile.repository.UserProfileRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
public class UserProfileService {
    UserProfileRepository userProfileRepository;
    UserProfileMapper profileMapper;

    public UserProfileResponse createProfile(UserProfileCreationRequest request) {
        UserProfile userProfile = profileMapper.toUserProfile(request);
        userProfileRepository.save(userProfile);
        return profileMapper.toUserProfileResponse(userProfile);
    }

    public UserProfileResponse getProfile(String userProfileId) {
        UserProfile userProfile = userProfileRepository.findById(userProfileId)
                .orElseThrow(() -> new RuntimeException("Cannot find profile"));
        return profileMapper.toUserProfileResponse(userProfile);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<UserProfileResponse> getProfileList() {
        List<UserProfile> userProfiles= userProfileRepository.findAll();
        return userProfiles.stream().map(profileMapper::toUserProfileResponse).toList();
    }

    public UserProfileResponse updateProfile(String id, UserProfileCreationRequest updateData) {
        log.info(updateData.toString());
        UserProfile existingProfile = userProfileRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(("Cannot find profile with id:" + id)));
        profileMapper.updateProfile(existingProfile, updateData);
        userProfileRepository.save(existingProfile);
        return profileMapper.toUserProfileResponse(existingProfile);
    }

    public void deleteProfile(String id) {
        userProfileRepository.deleteById(id);
    }
}
