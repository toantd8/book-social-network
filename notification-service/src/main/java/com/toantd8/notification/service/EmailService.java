package com.toantd8.notification.service;

import com.toantd8.notification.dto.request.EmailRequest;
import com.toantd8.notification.dto.request.SendEmailRequest;
import com.toantd8.notification.dto.request.Sender;
import com.toantd8.notification.dto.response.EmailResponse;
import com.toantd8.notification.exception.AppException;
import com.toantd8.notification.exception.ErrorCode;
import com.toantd8.notification.repository.httpclient.EmailClient;
import feign.FeignException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
public class EmailService {
    EmailClient emailClient;

    String apiKey = "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";
    public EmailResponse sendEmail(SendEmailRequest request) {
        EmailRequest emailRequest = EmailRequest.builder()
                .sender(Sender.builder()
                        .name("Duc Toan")
                        .email("ttoan2699@gmail.com")
                        .build())
                .to(List.of(request.getTo()))
                .subject(request.getSubject())
                .htmlContent(request.getHtmlContent())
                .build();
        try {
            return emailClient.sendEmail(apiKey, emailRequest);
        } catch (FeignException.FeignClientException ex) {
            throw new AppException(ErrorCode.CANNOT_SEND_EMAIL);
        }
    }
}
