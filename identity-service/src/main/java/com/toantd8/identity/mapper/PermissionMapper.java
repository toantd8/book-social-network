package com.toantd8.identity.mapper;

import org.mapstruct.Mapper;

import com.toantd8.identity.dto.request.PermissionRequest;
import com.toantd8.identity.dto.response.PermissionResponse;
import com.toantd8.identity.entity.Permission;

@Mapper(componentModel = "spring")
public interface PermissionMapper {
    Permission toPermission(PermissionRequest request);

    PermissionResponse toPermissionResponse(Permission permission);
}
