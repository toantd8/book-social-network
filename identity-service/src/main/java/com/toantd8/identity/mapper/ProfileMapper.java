package com.toantd8.identity.mapper;

import com.toantd8.identity.dto.request.ProfileCreationRequest;
import com.toantd8.identity.dto.request.UserCreationRequest;
import com.toantd8.identity.dto.request.UserUpdateRequest;
import com.toantd8.identity.dto.response.UserResponse;
import com.toantd8.identity.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ProfileMapper {
    ProfileCreationRequest toProfileCreationRequest(UserCreationRequest request);

}
